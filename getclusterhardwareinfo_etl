set hive.exec.reducers.bytes.per.reducer = 20971520;


insert overwrite table aiq.getclusterhardwareinfo
PARTITION (ds)

SELECT unioned.*
FROM
(
SELECT 
 createdon,
 CONCAT_WS(' ', substr(ingestedtime, 0, 10), substr(ingestedtime, 12, 8)) as ingestedtime,
 substr(ingestedtime, 0, 4),
 substr(ingestedtime, 6, 2),
 substr(ingestedtime, 9, 2),
 substr(ingestedtime, 12, 2),
 substr(ingestedtime, 15, 2),
 substr(ingestedtime, 18, 2),

 a.customeruid,
 a.customername,
 a.clusteruid,
 a.clustername,
 a.clusterversion,
 a.nodeid,
  
 get_json_object(node, "$.systemmemory.total") as total_systemmemory,
 get_json_object(node, "$.systemmemory.used") as used_systemmemory,
 get_json_object(node, "$.systemmemory.free") as free_systemmemory,
 get_json_object(node, "$.platform.nodetype") as platform_nodetype,
 get_json_object(node, "$.platform.cpumodel") as platform_cpumodel,
 get_json_object(node, "$.platform.nodememorygb") as platform_nodememorygb,
 get_json_object(node, "$.platform.chassistype") as platform_chassistype,
 get_json_object(system_value, "$.serial") as servicetag,
 CASE 
   WHEN get_json_object(node, '$.nvram.status') IS NOT NULL then 'Marvell'
   WHEN get_json_object(node, '$.nvram.extended') IS NOT NULL then 'Radian'
 ELSE NULL END AS nvram_vendor,
  
 nvram_errors_numoferrorlogentries,
 nvram_identify_serialnumber,
 nvram_identify_modelnumber,
 nvram_identify_firmwareversion,
 nvram_identify_hardwarerevision,
 nvram_extended_initialcapacitance,
 nvram_extended_initialesr,
 nvram_extended_dialogversion,
 nvram_extended_snapshottime,

 nvram_smart_temperature,
 nvram_extended_smartcounter_numberOfPowerCycles,
 nvram_extended_smartcounter_powerOnHours,
 nvram_extended_smartcounter_unsafeShutdowns,
 nvram_extended_smartcounter_mediaErrors,
 nvram_extended_smartcounter_numberOfErrorLogs,
 nvram_extended_measurement_enterpriseFlashControllerTemperature,
 nvram_extended_measurement_capacitor1And2Temperature,
 nvram_extended_measurement_capacitor3And4Temperature,
 nvram_extended_measurement_rearVentAmbientTemperature,
 nvram_extended_measurement_rms200BoardTemperature,
 nvram_extended_measurement_derivedEnergy,
 nvram_extended_measurement_derivedCapacitanceOfThePack,
 nvram_extended_measurement_derivedEsrOfCapacitorPack,
 nvram_extended_measurement_fanInletAmbientTemperature,

nvram_supercap_discharge,
 nvram_write_ready,
 nvram_supercap_charging,
 nvram_ssd_connected,
 nvram_supercap_connected,
 nvram_online_healthcheck,
 nvram_data_is_valid,
 nvram_current_temperature,
 nvram_minimum_temperature,
 nvram_average_temperature,
 nvram_alerttemperature_temperature,
 nvram_eeprom_revision,
 nvram_firmware_version,
 nvram_board_revision,
 nvram_ddr_configuration_revision,
 nvram_driver_version,
 nvram_ssd_firmware,
 nvram_device_serial_number,
 nvram_supercap_status,
 nvram_supercap_maximum_voltage,
 nvram_supercap_design_cap,
 nvram_supercap_trim_voltage,
 nvram_supercap_current_charge,
 nvram_supercap_charge_percent,
 nvram_supercap_capacitance_percent,
 nvram_supercap_minimum_voltage,
 nvram_supercap_capacitance,
 nvram_supercap_health,
 nvram_supercap_next_healthcheck,
 nvram_supercap_expected_charge,
 nvram_backup_end,
 nvram_progress_0_percent,
 nvram_supercap_duration,
 nvram_backup_status,
 nvram_supercap_start_voltage,
 nvram_ssd_flush_time,
 nvram_dram_multibit_errors,
 nvram_ssd_defective_blocks,
 data,
 ds

FROM
(
select 
   get_json_object(customer, "$.customeruid") as customeruid,
   get_json_object(customer, "$.customername") as customername,
   get_json_object(cluster, "$.clusteruid") as clusteruid,
   get_json_object(cluster, "$.clustername") as clustername,
   get_json_object(cluster, "$.clusterversion") as clusterversion,
   createdon,
   ingestedtime,
   data,
   ds,
   nodeid,
   node,
   get_json_object(node, '$.system') as system,
   get_json_object(`x`.`node`, '$.nvram.errors.numoferrorlogentries') as nvram_errors_numoferrorlogentries, 
   get_json_object(`x`.`node`, '$.nvram.identify.serialnumber') as nvram_identify_serialnumber,
   get_json_object(`x`.`node`, '$.nvram.identify.modelnumber') as nvram_identify_modelnumber,
   get_json_object(`x`.`node`, '$.nvram.identify.firmwareversion') as nvram_identify_firmwareversion,
   get_json_object(`x`.`node`, '$.nvram.identify.hardwarerevision') as nvram_identify_hardwarerevision,
   get_json_object(`x`.`node`, '$.nvram.extended.initialcapacitance') as nvram_extended_initialcapacitance,
   get_json_object(`x`.`node`, '$.nvram.extended.initialesr') as nvram_extended_initialesr,
   get_json_object(`x`.`node`, '$.nvram.extended.dialogversion') as nvram_extended_dialogversion,
   get_json_object(`x`.`node`, '$.nvram.extended.snapshottime')as nvram_extended_snapshottime,
   get_json_object(`x`.`node`, '$.nvram.extended.smartcounters') as nvram_extended_smartcounters,
   get_json_object(`x`.`node`, '$.nvram.smart.temperature') as nvram_smart_temperature,
   get_json_object(`x`.`node`, '$.nvram.status.supercapdischarge') as nvram_supercap_discharge,
   get_json_object(`x`.`node`, '$.nvram.status.writeready') as nvram_write_ready,
   get_json_object(`x`.`node`, '$.nvram.status.supercapcharging') as nvram_supercap_charging,
   get_json_object(`x`.`node`, '$.nvram.status.ssdconnected') as nvram_ssd_connected,
   get_json_object(`x`.`node`, '$.nvram.status.supercapconnected') as nvram_supercap_connected,
   get_json_object(`x`.`node`, '$.nvram.status.onlinehealthcheck') as nvram_online_healthcheck,
   get_json_object(`x`.`node`, '$.nvram.status.datavalid') as nvram_data_is_valid,
   get_json_object(`x`.`node`, '$.nvram.temperature.current') as nvram_current_temperature,
   get_json_object(`x`.`node`, '$.nvram.temperature.minimum') as nvram_minimum_temperature,
   get_json_object(`x`.`node`, '$.nvram.temperature.average') as nvram_average_temperature,
   get_json_object(`x`.`node`, '$.nvram.temperature.alerttemperature') as nvram_alerttemperature_temperature,
   get_json_object(`x`.`node`, '$.nvram.versions.eepromrevision') as nvram_eeprom_revision,
   get_json_object(`x`.`node`, '$.nvram.versions.firmware') as nvram_firmware_version,
   get_json_object(`x`.`node`, '$.nvram.versions.boardrevision') as nvram_board_revision,
   get_json_object(`x`.`node`, '$.nvram.versions.ddrconfigurationrevision') as nvram_ddr_configuration_revision,
   get_json_object(`x`.`node`, '$.nvram.versions.driver') as nvram_driver_version,
   get_json_object(`x`.`node`, '$.nvram.versions.ssdfirmware') as nvram_ssd_firmware,
   get_json_object(`x`.`node`, '$.nvram.serialnumbers.device') as nvram_device_serial_number,
   get_json_object(`x`.`node`, '$.nvram.supercap.status') as nvram_supercap_status,
   get_json_object(`x`.`node`, '$.nvram.supercap.maximumvoltage') as nvram_supercap_maximum_voltage,
   get_json_object(`x`.`node`, '$.nvram.supercap.designcap') as nvram_supercap_design_cap,
   get_json_object(`x`.`node`, '$.nvram.supercap.trimvoltage') as nvram_supercap_trim_voltage,
   get_json_object(`x`.`node`, '$.nvram.supercap.currentcharge') as nvram_supercap_current_charge,
   get_json_object(`x`.`node`, '$.nvram.supercap.chargepercent') as nvram_supercap_charge_percent,
   get_json_object(`x`.`node`, '$.nvram.supercap.capacitancepercent') as nvram_supercap_capacitance_percent,
   get_json_object(`x`.`node`, '$.nvram.supercap.minimumvoltage') as nvram_supercap_minimum_voltage,
   get_json_object(`x`.`node`, '$.nvram.supercap.capacitance') as nvram_supercap_capacitance,
   get_json_object(`x`.`node`, '$.nvram.supercap.health') as nvram_supercap_health,
   get_json_object(`x`.`node`, '$.nvram.supercap.nexthealthcheck') as nvram_supercap_next_healthcheck,
   get_json_object(`x`.`node`, '$.nvram.supercap.expectedcharge') as nvram_supercap_expected_charge,
   get_json_object(`x`.`node`, '$.nvram.backuplog.backupend') as nvram_backup_end,
   get_json_object(`x`.`node`, '$.nvram.backuplog.progress0percent') as nvram_progress_0_percent,
   get_json_object(`x`.`node`, '$.nvram.backuplog.supercapduration') as nvram_supercap_duration,
   get_json_object(`x`.`node`, '$.nvram.backuplog.backupstatus') as nvram_backup_status,
   get_json_object(`x`.`node`, '$.nvram.backuplog.supercapstartvoltage') as nvram_supercap_start_voltage,
   get_json_object(`x`.`node`, '$.nvram.backuplog.ssdflushtime') as nvram_ssd_flush_time,
   get_json_object(`x`.`node`, '$.nvram.backuplog.drammultibiterrors') as nvram_dram_multibit_errors,
   get_json_object(`x`.`node`, '$.nvram.ssd.defectiveblocks') as nvram_ssd_defective_blocks,
 
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'numberOfPowerCycles')), '$.value') as nvram_extended_smartcounter_numberOfPowerCycles,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'powerOnHours')), '$.value') as nvram_extended_smartcounter_powerOnHours,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'unsafeShutdowns')), '$.value') as nvram_extended_smartcounter_unsafeShutdowns,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'mediaErrors')), '$.value') as nvram_extended_smartcounter_mediaErrors,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'numberOfErrorLogs')), '$.value') as nvram_extended_smartcounter_numberOfErrorLogs,
   
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'enterpriseFlashControllerTemperature')), '$.recent') as nvram_extended_measurement_enterpriseFlashControllerTemperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'capacitor1And2Temperature')), '$.recent') as nvram_extended_measurement_capacitor1And2Temperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'capacitor3And4Temperature')), '$.recent') as nvram_extended_measurement_capacitor3And4Temperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'rearVentAmbientTemperature')), '$.recent') as nvram_extended_measurement_rearVentAmbientTemperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'rms200BoardTemperature')), '$.recent') as nvram_extended_measurement_rms200BoardTemperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'derivedEnergy')), '$.recent') as nvram_extended_measurement_derivedEnergy,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'derivedCapacitanceOfThePack')), '$.recent') as nvram_extended_measurement_derivedCapacitanceOfThePack,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'derivedEsrOfCapacitorPack')), '$.recent') as nvram_extended_measurement_derivedEsrOfCapacitorPack,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'fanInletAmbientTemperature')), '$.recent') as nvram_extended_measurement_fanInletAmbientTemperature
   
  

   
    FROM aiq.raw_optimized
	LATERAL VIEW EXPLODE 
	(
	  json_map(
		get_json_object(data, "$.result.clusterhardwareinfo.nodes"),
		'string,string'
	  )
	)x as nodeid, node
  WHERE ds = date_sub(CURRENT_DATE(), 1)
  AND source = "GetClusterHardwareInfo"
  AND node not like '%message%'
  AND get_json_object(node, '$.uuid') is not null
  AND get_json_object(node, '$.system') is not null 
) a
LATERAL VIEW EXPLODE(
  json_map(system, 'string,string')
  )p as system_key, system_value

UNION ALL 

SELECT 
 createdon,
 CONCAT_WS(' ', substr(ingestedtime, 0, 10), substr(ingestedtime, 12, 8)) as ingestedtime,
 substr(ingestedtime, 0, 4),
 substr(ingestedtime, 6, 2),
 substr(ingestedtime, 9, 2),
 substr(ingestedtime, 12, 2),
 substr(ingestedtime, 15, 2),
 substr(ingestedtime, 18, 2),

 a.customeruid,
 a.customername,
 a.clusteruid,
 a.clustername,
 a.clusterversion,
 a.nodeid,
  
 get_json_object(node, "$.systemmemory.total") as total_systemmemory,
 get_json_object(node, "$.systemmemory.used") as used_systemmemory,
 get_json_object(node, "$.systemmemory.free") as free_systemmemory,
 get_json_object(node, "$.platform.nodetype") as platform_nodetype,
 get_json_object(node, "$.platform.cpumodel") as platform_cpumodel,
 get_json_object(node, "$.platform.nodememorygb") as platform_nodememorygb,
 get_json_object(node, "$.platform.chassistype") as platform_chassistype,
 NULL as servicetag,
 CASE 
   WHEN get_json_object(node, '$.nvram.status') IS NOT NULL then 'Marvell'
   WHEN get_json_object(node, '$.nvram.extended') IS NOT NULL then 'Radian'
 ELSE NULL END AS nvram_vendor,
  
 nvram_errors_numoferrorlogentries,
 nvram_identify_serialnumber,
 nvram_identify_modelnumber,
 nvram_identify_firmwareversion,
 nvram_identify_hardwarerevision,
 nvram_extended_initialcapacitance,
 nvram_extended_initialesr,
 nvram_extended_dialogversion,
 nvram_extended_snapshottime,

 nvram_smart_temperature,
 nvram_extended_smartcounter_numberOfPowerCycles,
 nvram_extended_smartcounter_powerOnHours,
 nvram_extended_smartcounter_unsafeShutdowns,
 nvram_extended_smartcounter_mediaErrors,
 nvram_extended_smartcounter_numberOfErrorLogs,
 nvram_extended_measurement_enterpriseFlashControllerTemperature,
 nvram_extended_measurement_capacitor1And2Temperature,
 nvram_extended_measurement_capacitor3And4Temperature,
 nvram_extended_measurement_rearVentAmbientTemperature,
 nvram_extended_measurement_rms200BoardTemperature,
 nvram_extended_measurement_derivedEnergy,
 nvram_extended_measurement_derivedCapacitanceOfThePack,
 nvram_extended_measurement_derivedEsrOfCapacitorPack,
 nvram_extended_measurement_fanInletAmbientTemperature,

 nvram_supercap_discharge,
 nvram_write_ready,
 nvram_supercap_charging,
 nvram_ssd_connected,
 nvram_supercap_connected,
 nvram_online_healthcheck,
 nvram_data_is_valid,
 nvram_current_temperature,
 nvram_minimum_temperature,
 nvram_average_temperature,
 nvram_alerttemperature_temperature,
 nvram_eeprom_revision,
 nvram_firmware_version,
 nvram_board_revision,
 nvram_ddr_configuration_revision,
 nvram_driver_version,
 nvram_ssd_firmware,
 nvram_device_serial_number,
 nvram_supercap_status,
 nvram_supercap_maximum_voltage,
 nvram_supercap_design_cap,
 nvram_supercap_trim_voltage,
 nvram_supercap_current_charge,
 nvram_supercap_charge_percent,
 nvram_supercap_capacitance_percent,
 nvram_supercap_minimum_voltage,
 nvram_supercap_capacitance,
 nvram_supercap_health,
 nvram_supercap_next_healthcheck,
 nvram_supercap_expected_charge,
 nvram_backup_end,
 nvram_progress_0_percent,
 nvram_supercap_duration,
 nvram_backup_status,
 nvram_supercap_start_voltage,
 nvram_ssd_flush_time,
 nvram_dram_multibit_errors,
 nvram_ssd_defective_blocks,
 data, 
 ds

FROM
(
select 
   get_json_object(customer, "$.customeruid") as customeruid,
   get_json_object(customer, "$.customername") as customername,
   get_json_object(cluster, "$.clusteruid") as clusteruid,
   get_json_object(cluster, "$.clustername") as clustername,
   get_json_object(cluster, "$.clusterversion") as clusterversion,
   createdon,
   ingestedtime,
   data,
   ds,
   nodeid,
   node,
   get_json_object(node, '$.system') as system,
   get_json_object(`x`.`node`, '$.nvram.errors.numoferrorlogentries') as nvram_errors_numoferrorlogentries, 
   get_json_object(`x`.`node`, '$.nvram.identify.serialnumber') as nvram_identify_serialnumber,
   get_json_object(`x`.`node`, '$.nvram.identify.modelnumber') as nvram_identify_modelnumber,
   get_json_object(`x`.`node`, '$.nvram.identify.firmwareversion') as nvram_identify_firmwareversion,
   get_json_object(`x`.`node`, '$.nvram.identify.hardwarerevision') as nvram_identify_hardwarerevision,
   get_json_object(`x`.`node`, '$.nvram.extended.initialcapacitance') as nvram_extended_initialcapacitance,
   get_json_object(`x`.`node`, '$.nvram.extended.initialesr') as nvram_extended_initialesr,
   get_json_object(`x`.`node`, '$.nvram.extended.dialogversion') as nvram_extended_dialogversion,
   get_json_object(`x`.`node`, '$.nvram.extended.snapshottime')as nvram_extended_snapshottime,
   get_json_object(`x`.`node`, '$.nvram.extended.smartcounters') as nvram_extended_smartcounters,
   get_json_object(`x`.`node`, '$.nvram.smart.temperature') as nvram_smart_temperature,
   get_json_object(`x`.`node`, '$.nvram.status.supercapdischarge') as nvram_supercap_discharge,
   get_json_object(`x`.`node`, '$.nvram.status.writeready') as nvram_write_ready,
   get_json_object(`x`.`node`, '$.nvram.status.supercapcharging') as nvram_supercap_charging,
   get_json_object(`x`.`node`, '$.nvram.status.ssdconnected') as nvram_ssd_connected,
   get_json_object(`x`.`node`, '$.nvram.status.supercapconnected') as nvram_supercap_connected,
   get_json_object(`x`.`node`, '$.nvram.status.onlinehealthcheck') as nvram_online_healthcheck,
   get_json_object(`x`.`node`, '$.nvram.status.datavalid') as nvram_data_is_valid,
   get_json_object(`x`.`node`, '$.nvram.temperature.current') as nvram_current_temperature,
   get_json_object(`x`.`node`, '$.nvram.temperature.minimum') as nvram_minimum_temperature,
   get_json_object(`x`.`node`, '$.nvram.temperature.average') as nvram_average_temperature,
   get_json_object(`x`.`node`, '$.nvram.temperature.alerttemperature') as nvram_alerttemperature_temperature,
   get_json_object(`x`.`node`, '$.nvram.versions.eepromrevision') as nvram_eeprom_revision,
   get_json_object(`x`.`node`, '$.nvram.versions.firmware') as nvram_firmware_version,
   get_json_object(`x`.`node`, '$.nvram.versions.boardrevision') as nvram_board_revision,
   get_json_object(`x`.`node`, '$.nvram.versions.ddrconfigurationrevision') as nvram_ddr_configuration_revision,
   get_json_object(`x`.`node`, '$.nvram.versions.driver') as nvram_driver_version,
   get_json_object(`x`.`node`, '$.nvram.versions.ssdfirmware') as nvram_ssd_firmware,
   get_json_object(`x`.`node`, '$.nvram.serialnumbers.device') as nvram_device_serial_number,
   get_json_object(`x`.`node`, '$.nvram.supercap.status') as nvram_supercap_status,
   get_json_object(`x`.`node`, '$.nvram.supercap.maximumvoltage') as nvram_supercap_maximum_voltage,
   get_json_object(`x`.`node`, '$.nvram.supercap.designcap') as nvram_supercap_design_cap,
   get_json_object(`x`.`node`, '$.nvram.supercap.trimvoltage') as nvram_supercap_trim_voltage,
   get_json_object(`x`.`node`, '$.nvram.supercap.currentcharge') as nvram_supercap_current_charge,
   get_json_object(`x`.`node`, '$.nvram.supercap.chargepercent') as nvram_supercap_charge_percent,
   get_json_object(`x`.`node`, '$.nvram.supercap.capacitancepercent') as nvram_supercap_capacitance_percent,
   get_json_object(`x`.`node`, '$.nvram.supercap.minimumvoltage') as nvram_supercap_minimum_voltage,
   get_json_object(`x`.`node`, '$.nvram.supercap.capacitance') as nvram_supercap_capacitance,
   get_json_object(`x`.`node`, '$.nvram.supercap.health') as nvram_supercap_health,
   get_json_object(`x`.`node`, '$.nvram.supercap.nexthealthcheck') as nvram_supercap_next_healthcheck,
   get_json_object(`x`.`node`, '$.nvram.supercap.expectedcharge') as nvram_supercap_expected_charge,
   get_json_object(`x`.`node`, '$.nvram.backuplog.backupend') as nvram_backup_end,
   get_json_object(`x`.`node`, '$.nvram.backuplog.progress0percent') as nvram_progress_0_percent,
   get_json_object(`x`.`node`, '$.nvram.backuplog.supercapduration') as nvram_supercap_duration,
   get_json_object(`x`.`node`, '$.nvram.backuplog.backupstatus') as nvram_backup_status,
   get_json_object(`x`.`node`, '$.nvram.backuplog.supercapstartvoltage') as nvram_supercap_start_voltage,
   get_json_object(`x`.`node`, '$.nvram.backuplog.ssdflushtime') as nvram_ssd_flush_time,
   get_json_object(`x`.`node`, '$.nvram.backuplog.drammultibiterrors') as nvram_dram_multibit_errors,
   get_json_object(`x`.`node`, '$.nvram.ssd.defectiveblocks') as nvram_ssd_defective_blocks,
 
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'numberOfPowerCycles')), '$.value') as nvram_extended_smartcounter_numberOfPowerCycles,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'powerOnHours')), '$.value') as nvram_extended_smartcounter_powerOnHours,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'unsafeShutdowns')), '$.value') as nvram_extended_smartcounter_unsafeShutdowns,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'mediaErrors')), '$.value') as nvram_extended_smartcounter_mediaErrors,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.smartcounters'), 'name', 'numberOfErrorLogs')), '$.value') as nvram_extended_smartcounter_numberOfErrorLogs,
   
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'enterpriseFlashControllerTemperature')), '$.recent') as nvram_extended_measurement_enterpriseFlashControllerTemperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'capacitor1And2Temperature')), '$.recent') as nvram_extended_measurement_capacitor1And2Temperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'capacitor3And4Temperature')), '$.recent') as nvram_extended_measurement_capacitor3And4Temperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'rearVentAmbientTemperature')), '$.recent') as nvram_extended_measurement_rearVentAmbientTemperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'rms200BoardTemperature')), '$.recent') as nvram_extended_measurement_rms200BoardTemperature,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'derivedEnergy')), '$.recent') as nvram_extended_measurement_derivedEnergy,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'derivedCapacitanceOfThePack')), '$.recent') as nvram_extended_measurement_derivedCapacitanceOfThePack,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'derivedEsrOfCapacitorPack')), '$.recent') as nvram_extended_measurement_derivedEsrOfCapacitorPack,
   get_json_object((json_array_filter(get_json_object(node, '$.nvram.extended.measurement'), 'name', 'fanInletAmbientTemperature')), '$.recent') as nvram_extended_measurement_fanInletAmbientTemperature
   
  

   
    FROM aiq.raw_optimized
	LATERAL VIEW EXPLODE 
	(
	  json_map(
		get_json_object(data, "$.result.clusterhardwareinfo.nodes"),
		'string,string'
	  )
	)x as nodeid, node
  WHERE ds = date_sub(CURRENT_DATE(), 1)
  AND source = "GetClusterHardwareInfo"
  AND node not like '%message%'
  AND get_json_object(node, '$.uuid') is not null
  AND get_json_object(node, '$.system') is  null
) a
)unioned
DISTRIBUTE BY clusteruid, nodeid
SORT BY ingestedtime
